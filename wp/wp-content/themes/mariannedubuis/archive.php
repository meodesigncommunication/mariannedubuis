<?php

global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );


if ( empty( $page_layout ) ) {
	$page_layout = 'full';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

get_header('blog'); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
            <div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage"> 
                <section class="bloc-posts">
                    <?php
                        // The Query
                        $posts = query_posts( 'posts_per_page=10' );
                        // The Loop
                        while ( have_posts() ) : the_post(); ?>
                            <?php                             
                                $id = get_the_ID();
                                $size = 'full';
                                $featuredImageId = get_post_thumbnail_id($id);        
                                $imageUrl = wp_get_attachment_image_src($featuredImageId, $size); 
                                $content = $post->post_content;
                            ?>
                            <article class="bloc-post">
                                <div class="image-post">
                                    <img src="<?php echo $imageUrl[0] ?>" alt="" />
                                </div>
                                <div class="content-post">
                                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h2>
                                    <p class="meta-data">Catégorie <span><?php the_category(', ') ?></span> - posté le <span><?php the_time('j F Y') ?></span></p>
                                    <p class="content">
                                        <?php the_excerpt() ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" title="Voir plus" class="btn-show-more">Lire plus</a>
                                </div>
                            </article>
                        <?php endwhile;
                        // Reset Query
                        wp_reset_query();

                    ?>
                </section>
            </div>
        </div>
    </div>	
</div>
<?php get_footer(); ?>
