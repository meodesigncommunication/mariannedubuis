<?php
/*
 * Template Name: Contact standard page
 */
global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );
$featuredImageId = get_post_thumbnail_id($post->ID);        
$imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');  


if ( empty( $page_layout ) ) {
	$page_layout = 'full';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

get_header(); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
	<div class="mk-main-wrapper-holder">
		<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
			<div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage">                          
				<?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
						<?php the_content();?>
						<div class="clearboth"></div>
						<?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
				<?php endwhile; ?>
						<?php
						if($mk_options['pages_comments'] == 'true') {
							if ( comments_open() ) :
							comments_template( '', true ); 	
							endif;
						}
						?>
				</div>
			
		<?php if ( $page_layout != 'full' ) get_sidebar(); ?>
		<div class="clearboth"></div>
		</div>
		<div class="clearboth"></div>
	</div>	
</div>
<script type="text/javascript">
    window.$ = jQuery 
    $(document).ready(function(){
        // Put de feature image in title bloc
        var background = 'transparent url("<?php echo $imageUrl[0] ?>") no-repeat';
        $('#mk-header .mk-zindex-fix').css('background',background);
        $('#mk-header .mk-zindex-fix').css('background-size','cover');
        resizeTitleHeader();
    });
    function resizeTitleHeader()
    {
        var ratio = (<?php echo $imageUrl[1] ?>/<?php echo $imageUrl[2] ?>);
        var pageTest = '<?php echo the_title() ?>';
        if(pageTest != 'contact' && pageTest != 'Contact')
        {
            var maxHeight = 550;
        }else{
            var maxHeight = 200;
        }
        var height = ($(window).width()/ratio);
        if(height > maxHeight)
        {
            height = maxHeight;
        }else{
            height = maxHeight;
        }
        $('#mk-header .mk-zindex-fix').css('height',height);
        $('#mk-header #mk-page-introduce').css('height',height);
        $('#mk-header #mk-page-introduce .mk-grid').css('height',height);
    }
</script>
<?php get_footer('contact'); ?>

