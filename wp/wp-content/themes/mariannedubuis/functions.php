<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() 
{
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

/**
 * 
 * Créer un design personalisé d'un menu avec image feature des pages du menu
 * 
 * @param require string $menu [nom du menu créé dans l'admin]
 * @param facultatif string $size [full, large, medium, thumbnail] default full
 * 
 * @return HTML menu
 */
function generate_projects_menu($menu, $size = 'full')
{
    $html = '';
    
    $posts_menu = wp_get_nav_menu_items($menu);
    
    $classCol = 'col-md-4 nav-element';
    
    $html .= '<nav id="nav-project">';
    $html .= '<div class="row global-nav">';
    
    foreach($posts_menu as $post_menu)
    {
       $post_id = $post_menu->object_id;       
       $featuredImageId = get_post_thumbnail_id($post_id);        
       $imageUrl = wp_get_attachment_image_src($featuredImageId, $size);       
       $title = get_the_title($post_id);
       $style = 'background-image:url('.$imageUrl[0].');';

       $html .= '<div style="'.$style.'" class="'.$classCol.'">';
       $html .=     '<a href="'.$post_menu->url.'" title="'.$title.'"><h2>'.$title.'</h2></a>';       
       $html .= '</div>';       
    }
    
    $html .= '</div>';
    $html .= '</nav>';
    
    return $html;
}

function pagination($query) {  
	
	$baseURL="http://".$_SERVER['HTTP_HOST'];
	if($_SERVER['REQUEST_URI'] != "/"){
		$baseURL = $baseURL.$_SERVER['REQUEST_URI'];
	}
 
	// Suppression de '/page' de l'URL
	$sep = strrpos($baseURL, '/page/');
	if($sep != FALSE){
		$baseURL = substr($baseURL, 0, $sep);
	}
 
  // Suppression des paramètres de l'URL
	$sep = strrpos($baseURL, '?');
	if($sep != FALSE){
	// On supprime le caractère avant qui est un '/'
		$baseURL = substr($baseURL, 0, ($sep-1));
	}	
	
	$page = $query->query_vars["paged"];  
	if ( !$page ) $page = 1;  
	$qs = $_SERVER["QUERY_STRING"] ? "?".$_SERVER["QUERY_STRING"] : "";  
	
	// Nécessaire uniquement si on a plus de posts que de posts par page admis 
	if ( $query->found_posts > $query->query_vars["posts_per_page"] ) {  
		echo '<ul class="pagination">'; 
		// lien précédent si besoin
		if ( $page > 1 ) { 
			echo '<li class="next_prev prev"><a title="Revenir à la page précédente (vous êtes à la page '.$page.')" href="'.$baseURL.'/page/'.($page-1).'/'.$qs.'">« précédente</a></li>'; 
		} 
		// la boucle pour les pages
		for ( $i=1; $i <= $query->max_num_pages; $i++ ) { 
			// ajout de la classe active pour la page en cours de visualisation
			if ( $i == $page ) { 
				echo '<li class="active"><a href="#pagination" title="Vous êtes sur cette page '.$i.'">'.$i.'</a></li>'; 
			} else { 
				echo '<li><a title="Rejoindre la page '.$i.'" href="'.$baseURL.'/page/'.$i.'/'.$qs.'">'.$i.'</a></li>'; 
			} 
		} 
		// le lien next si besoin
		if ( $page < $query->max_num_pages ) { 
			echo '<li class="next_prev next"><a title="Passer à la page suivante (vous êtes à la page '.$page.')" href="'.$baseURL.'/page/'.($page+1).'/'.$qs.'">suivante »</a></li>'; 
		} 
		echo '</ul>';  
	}  
}

add_filter('upload_mimes', 'custom_upload_mimes');

function custom_upload_mimes ( $existing_mimes=array() ) {

	// add the file extension to the array

	$existing_mimes['svg'] = 'mime/type';

        // call the modified list of extensions

	return $existing_mimes;

}