<?php
/*
 * Template Name: journal
 */
global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );
$post_per_page = 3;
$counter = 0;
$page_number = 1;


if ( empty( $page_layout ) ) {
	$page_layout = 'full';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

get_header('blog'); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
            <div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage"> 
                <section class="bloc-posts">
                    <div id="page_1" class="page_posts">
                        <?php
                            // The Query
                            $args = array(
                                'posts_per_page' => '200',
                                'order'    => 'DESC'
                            );
                            $posts = query_posts($args);
                            // The Loop
                            while ( have_posts() ) : the_post(); ?>
                                <?php                             
                                    $id = get_the_ID();
                                    $counter++;
                                    $size = 'full';
                                    $featuredImageId = get_post_thumbnail_id($id);        
                                    $imageUrl = wp_get_attachment_image_src($featuredImageId, $size); 
                                    $content = $post->post_content;
                                ?>
                                <article class="bloc-post">
                                    <div class="image-post">
                                        <img src="<?php echo $imageUrl[0] ?>" alt="" />
                                    </div>
                                    <div class="content-post">
                                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h2>
                                        <p class="meta-data">Catégorie <span><?php the_category(', ') ?></span> - posté le <span><?php the_time('j F Y') ?></span></p>
                                        <?php the_excerpt() ?>
                                        <a href="<?php the_permalink(); ?>" title="Voir plus" class="btn-show-more"><?php echo __('[:fr]Lire plus[:en]Read more') ?></a>
                                    </div>
                                </article>
                                <?php
                                    if(($counter%$post_per_page) == 0)
                                    {
                                        $page_number++;
                                        echo '</div>';
                                        echo '<div id="page_'.$page_number.'" class="page_posts">';
                                    }
                                ?>
                            <?php endwhile;
                            // Reset Query
                            wp_reset_query();
                        ?>
                    </div>
                    <input type="hidden" name="current_page" id="current_page" value="" />
                    <?php
                        echo '<input onClick="changePage(this)" type="button" name="first_page" class="btn_paginate" value="<<"/>';
                        echo '<input onClick="changePage(this)" type="button" name="previous_page" class="btn_paginate" value="<"/>';
                        for($i = 1; $i <= $page_number; $i++)
                        {
                            echo '<input onClick="changePage(this)" type="button" name="page_'.$i.'" class="btn_paginate" value="'.$i.'"/>';
                        }
                        echo '<input onClick="changePage(this)" type="button" name="next_page" class="btn_paginate" value=">"/>';
                        echo '<input onClick="changePage(this)"  type="button" name="last_page" class="btn_paginate" value=">>"/>';
                    ?> 
                </section>
            </div>
        </div>
    </div>	
</div>
<script type="text/javascript">
    window.$ = jQuery 
    $(document).ready(function(){
        $('#page_1').css('display','block');
        $('#current_page').val(1);
        refreshPaginateButton();
    });
    
    function refreshPaginateButton()
    {
        $('.bloc-posts .page_posts').each(function() {   
            if($(this).attr('id') == 'page_1' && $(this).css('display') != 'none')
            {
                $('input[name$="first_page"]').css('display','none');
                $('input[name$="previous_page"]').css('display','none');
                $('input[name$="next_page"]').css('display','inline-block');
                $('input[name$="last_page"]').css('display','inline-block');
                $('input[type$="button"]').each(function() 
                {
                    if($(this).hasClass('active'))
                    {
                        $(this).removeClass('active');
                    }
                });
                $('input[name$="'+$(this).attr('id')+'"]').addClass('active');
            }            
            else if($(this).attr('id') != 'page_1' && $(this).attr('id') != 'page_<?php echo $page_number; ?>' && $(this).css('display') != 'none')
            {
                $('input[name$="first_page"]').css('display','inline-block');
                $('input[name$="previous_page"]').css('display','inline-block');
                $('input[name$="next_page"]').css('display','inline-block');
                $('input[name$="last_page"]').css('display','inline-block');
                $('input[type$="button"]').each(function() 
                {
                    if($(this).hasClass('active'))
                    {
                        $(this).removeClass('active');
                    }
                });
                $('input[name$="'+$(this).attr('id')+'"]').addClass('active');
            }            
            else if($(this).attr('id') == 'page_<?php echo $page_number; ?>' && $(this).css('display') != 'none')
            {
                $('input[name$="first_page"]').css('display','inline-block');
                $('input[name$="previous_page"]').css('display','inline-block');
                $('input[name$="next_page"]').css('display','none');
                $('input[name$="last_page"]').css('display','none');
                $('input[type$="button"]').each(function() 
                {
                    if($(this).hasClass('active'))
                    {
                        $(this).removeClass('active');
                    }
                });
                $('input[name$="'+$(this).attr('id')+'"]').addClass('active');
            } 
        });
    }
    
    function changePage(input_click)
    {        
        var current_page = $('#current_page').val();
        $('#page_'+current_page).css('display', 'none');
        if($(input_click).attr('name') == 'first_page')
        {
            $('#page_1').css('display', 'block');
            $('#current_page').val(1);            
        }else if($(input_click).attr('name') == 'last_page'){
            $('#page_<?php echo $page_number ?>').css('display', 'block');
            $('#current_page').val(<?php echo $page_number ?>);            
        }else if($(input_click).attr('name') == 'previous_page'){
            var page = (current_page*1)-1;
            $('#page_'+page).css('display', 'block');
            $('#current_page').val(page);            
        }else if($(input_click).attr('name') == 'next_page'){
            var page = (current_page*1)+1;
            $('#page_'+page).css('display', 'block');
            $('#current_page').val(page);            
        }else{
            $('#'+$(input_click).attr('name')).css('display','block');
            $('#current_page').val($(input_click).val());
        }  
        refreshPaginateButton();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }    
</script>
<?php get_footer(); ?>