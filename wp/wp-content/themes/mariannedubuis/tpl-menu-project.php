<?php
/*
 * Template Name: Project menu
 */
global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );


if ( empty( $page_layout ) ) {
	$page_layout = 'full';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

get_header(); 
?>
<div id="theme-page" class="projects-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> vc_row-fluid">
            <div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage">
                <?php
                    
                    $menu = 'projects_menu';
                    
                    echo generate_projects_menu($menu);
                    
                ?>
            </div>
            <?php if ( $page_layout != 'full' ) get_sidebar(); ?>
            <div class="clearboth"></div>
        </div>
        <div class="clearboth"></div>
    </div>
</div>
<script type="text/javascript">
    window.$ = jQuery 
    $(document).ready(function(){
        resizeNav();
    });
    $(window).resize(function(){
        resizeNav();
    });
    function resizeNav()
    {
        var height = (($( window ).height()-$('header').height())/2);
        var padding = ((height/2)-30);
        $('.nav-element').height(height+"px");
        //$('.nav-element a h2').css('padding-top',padding+"px");
    }
</script>
<?php get_footer(); ?>