<?php
/*
 * Template Name: project
 */
global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );


if ( empty( $page_layout ) ) {
	$page_layout = 'full';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

get_header(); 
?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
            <div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage">
            <?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
                <?php the_content();?>
                <div class="clearboth"></div>
                <?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
            <?php endwhile; ?>
            <?php
                if($mk_options['pages_comments'] == 'true') {
                    if ( comments_open() ) :
                    comments_template( '', true ); 	
                    endif;
                }
            ?>
            </div>
            <?php if ( $page_layout != 'full' ) get_sidebar(); ?>
            <div class="clearboth"></div>
        </div>
        <div class="clearboth"></div>
    </div>
    <div id="btn-close-project">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/btn-close-project.png" alt="close" />
    </div>
    <div id="bloc-btn-info">
        <div id="btn-info-project">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/arrow_top.png" alt="Ouvrir les infos" />
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript">
    window.$ = jQuery 
    $(document).ready(function(){
        checkDescSwiper();
    });
    $(".mk-edge-slider .edge-slider-holder").swipe( {
        //Generic swipe handler for all directions
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            checkDescSwiper();
        }
    });
    
    function checkDescSwiper()
    {
        var content = $('.mk-edge-slider .swiper-slide-visible .edge-desc').html();
        //alert('content = '+content);
        if(typeof content === 'undefined')
        {
            $('#bloc-btn-info #btn-info-project').css('display','none');
        }else{
            $('#bloc-btn-info #btn-info-project').css('display','block');
        }
    }
    
    //***************************************************************************************
    $('#btn-close-project').click(function(){
        <?php 
        $list = explode('/wp', site_url());
        $full_url = '';
        foreach($list as $url)
        {
            $full_url .= $url;
        }
        $full_url .= '/projets';
        ?>
        window.location.href = "<?php echo $full_url;?>";
    });
    $('#bloc-btn-info #btn-info-project').click(function(){
        var css = $('.mk-edge-slider .edge-slide-content').css('display');
        if(css == 'none')
        {
            $('.mk-edge-slider .edge-slide-content').css('display','block');
            $('#bloc-btn-info #btn-info-project img').attr('src','<?php echo get_stylesheet_directory_uri() ?>/images/arrow_bottom.png');
        }else{
            $('.mk-edge-slider .edge-slide-content').css('display','none');
            $('#bloc-btn-info #btn-info-project img').attr('src','<?php echo get_stylesheet_directory_uri() ?>/images/arrow_top.png');
        }
    });
</script>
<?php get_footer(); ?>