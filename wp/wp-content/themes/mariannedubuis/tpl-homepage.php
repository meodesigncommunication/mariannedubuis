<?php
/*
 * Template Name: homepage
 */
global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );


if ( empty( $page_layout ) ) {
	$page_layout = 'full';
}
$padding = ($padding == 'true') ? 'no-padding' : '';

get_header(); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
        <div id="logo-homepage">
            <div class="logo-position">
                <img src="wp/wp-content/themes/mariannedubuis/images/logo-homepage.png" alt="Marianne Dubuis découpeuse" />
            </div>
        </div>
	<div class="mk-main-wrapper-holder">
		<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper <?php echo $page_layout; ?>-layout <?php echo $padding; ?> mk-grid vc_row-fluid">
			<div class="theme-content <?php echo $padding; ?>" itemprop="mainContentOfPage">
                            <div id="bloc-arrow-homepage">    
                                <div class="arrow-nav">
                                    <a href="<?php echo bloginfo('url').'/projets';?>" title="Portfolio">
                                        <div><?php echo __("[:fr]projets[:en]projects"); ?></div>
                                        <div><i class="glyphicon glyphicon-chevron-down"></i></div>
                                        <div class="clearboth"></div>
                                    </a>
                                </div>
                            </div>
                            <?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
                                            <?php the_content();?>
                                            <div class="clearboth"></div>
                                            <?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
                            <?php endwhile; ?>
                                            <?php
                                            if($mk_options['pages_comments'] == 'true') {
                                                if ( comments_open() ) :
                                                comments_template( '', true ); 	
                                                endif;
                                            }
                                            ?>
                            </div>			
		<?php if ( $page_layout != 'full' ) get_sidebar(); ?>                
		<div class="clearboth"></div>
            </div>
            <div class="clearboth"></div>
	</div>	
</div>
<script type="text/javascript">
    window.$ = jQuery 
    $(document).ready(function(){
        //
    });
    $('.edge-skip-slider').click(function(){
        var url = '<?php echo site_url('/projets') ?>';
        window.location.href = url;
    });
</script>
<?php get_footer(); ?>